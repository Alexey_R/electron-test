import {Entity, Column, PrimaryGeneratedColumn} from "typeorm";

@Entity({name: "photo"})
export class Photo {

  @PrimaryGeneratedColumn()
  id: number;

  @Column({
    length: 100
  })
  name2: string;

  @Column("text")
  description: string;

  @Column("text")
  description2: string;

  @Column()
  filename: string;

  @Column("double")
  views: number;

  @Column()
  isPublished: boolean;
}