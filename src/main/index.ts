import "reflect-metadata";
import { app, BrowserWindow } from "electron";
import * as path from "path";
import { format as formatUrl } from "url";
import { autoUpdater } from "electron-updater";
import log from "electron-log";
import { createConnection } from "typeorm";
import { Photo } from "./entity/Photo";
import { MigrationName1580485663176 } from "./migration/1580485663176-MigrationName";

app.setAppUserModelId("com.electron.app");
app.setAsDefaultProtocolClient("ps-softwarename");

const isDevelopment = process.env.NODE_ENV !== "production";

let mainWindow: BrowserWindow | null;

function createMainWindow() {
  const window = new BrowserWindow({
    webPreferences: { nodeIntegration: true }
  });

  if (isDevelopment) {
    window.webContents.openDevTools();
  }

  if (isDevelopment) {
    window.loadURL(`http://localhost:${process.env.ELECTRON_WEBPACK_WDS_PORT}`);
  } else {
    window.loadURL(
      formatUrl({
        pathname: path.join(__dirname, "index.html"),
        protocol: "file",
        slashes: true
      })
    );
  }

  window.on("closed", () => {
    mainWindow = null;
  });

  window.webContents.on("devtools-opened", () => {
    window.focus();
    setImmediate(() => {
      window.focus();
    });
  });

  return window;
}

// quit application when all windows are closed
app.on("window-all-closed", () => {
  // on macOS it is common for applications to stay open until the user explicitly quits
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  // on macOS it is common to re-create a window even after all windows have been closed
  if (mainWindow === null) {
    mainWindow = createMainWindow();
  }
});

// create main BrowserWindow when electron is ready
app.on("ready", () => {

  mainWindow = createMainWindow();

  createConnection({
    type: "mysql",
    host: "localhost",
    port: 3306,
    username: "root",
    password: "root",
    database: "electron_db",
    entities: [
      Photo
    ],
    migrations: [MigrationName1580485663176],
    migrationsRun: true,
    // synchronize: true,
    logging: false
  }).then(connection => {
    console.log(1)
  }).catch(error => console.log(error));

  //todo move to another place
  log.transports.file.level = "debug";
  autoUpdater.logger = log;
  autoUpdater.autoInstallOnAppQuit = true;
  autoUpdater.autoDownload = true;
  autoUpdater.checkForUpdatesAndNotify();
});
