import {MigrationInterface, QueryRunner} from "typeorm";

export class MigrationName1580485663176 implements MigrationInterface {
    name = 'MigrationName1580485663176'

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `photo` ADD `description2` text NOT NULL", undefined);
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `photo` DROP COLUMN `description2`", undefined);
    }

}
