import React, { FunctionComponent, useState } from "react";

export const Hi: FunctionComponent<any> = () => {
  const [count, setCount] = useState(0);
  return <div>
    <button onClick={() => setCount(s => s + 1)}>{count}</button>
  </div>
};