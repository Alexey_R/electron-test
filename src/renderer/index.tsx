import React from 'react';
import ReactDOM from 'react-dom';
import { Hi } from './Hi';

ReactDOM.render(<Hi/>, document.querySelector('#app'));